from rest_framework.views import APIView
from rest_framework.response import Response

# Models
from users.models import User

# Create your views here.


class UserView(APIView):
    def get(self,request):
        return Response({'Hello world'}, 200)

    def post(self, request):
        user = User.objects.create(
            **request.data
        )
        return Response({'user_id': user.id}, 200)
